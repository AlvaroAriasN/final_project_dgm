# Final_Project_DGM

Article chosen : High-Fidelity Synthesis with Disentangled Representation (https://arxiv.org/pdf/2001.04296.pdf)

For a better view of the steps, check the notebook.

## Steps

1. Load the celeba dataset and convert with dataloader.
2. Train the VAE with the dataloader
3. Train the GAN including the VAE to generate the fake images (feed result into generator)
4. Compute total generator loss as generator loss + vae loss using the vae for real and fake images

NOTE: Model ran with only one epoch to save computer time.
